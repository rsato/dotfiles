#######################################
# setting PATHes
#######################################

XDG_CONFIG_HOME="$XDG_CONFIG_HOMEH:~/.config"

# myformatter for python
PATH="${HOME}/myformatter/:$PATH"

export PYENV_ROOT="${HOME}/.pyenv"
if [ -d "${PYENV_ROOT}" ]; then
    export PATH=${PYENV_ROOT}/bin:$PATH
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi
