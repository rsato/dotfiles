# You have installed git, right?
# If you had done install git, install git.

echo '***************************************'
echo 'start init settings ubuntu'
echo '***************************************'

echo '***************************************'
echo 'dir name: jp -> en'
echo '***************************************'
LANG=C xdg-user-dirs-gtk-update


echo '***************************************'
echo 'Install zsh'
echo '***************************************'
sudo apt-get -y install zsh
chsh

echo '***************************************'
echo 'Install tmux'
echo '***************************************'
sudo apt-get -y install tmux

echo '***************************************'
echo 'Install vim-gtk'
echo '***************************************'
sudo apt-get -y install vim-gtk

echo '***************************************'
echo 'Setting dotfiles'
echo '***************************************'
cd $HOME/dotfiles
zsh setup.sh

echo '***************************************'
echo 'Setting icon '
echo '***************************************'
sudo add-apt-repository ppa:numix/ppa
sudo apt-get update
sudo apt-get install numix-icon-theme-circle
sudo apt-get install numix-gtk-theme
