nnoremap m <Nop>
let mapleader = 'm'
let $PATH = '~/.pyenv/shims:'.$PATH


" ##############################################
" dein settings
" ##############################################

" プラグインがインストールされるディレクトリ
let s:dein_dir = expand('~/.cache/dein') " dein.vim 本体
let s:dein_repo_dir = s:dein_dir . '/repos/github.com/Shougo/dein.vim'

" dein.vim がなければ github から落としてくる
if &runtimepath !~# '/dein.vim'
  if !isdirectory(s:dein_repo_dir)
    execute '!git clone https://github.com/Shougo/dein.vim' s:dein_repo_dir
  endif
  execute 'set runtimepath^=' . fnamemodify(s:dein_repo_dir, ':p')
endif

" 設定開始
if dein#load_state(s:dein_dir)
  call dein#begin(s:dein_dir)
  " プラグインリストを収めた TOML ファイル
  " 予め TOML ファイルを用意しておく
  let g:rc_dir    = expand("~/.vim/rc/")
  let s:toml      = g:rc_dir . '/dein.toml'
  let s:lazy_toml = g:rc_dir . '/dein_lazy.toml'
" TOML を読み込み、キャッシュしておく
  call dein#load_toml(s:toml,      {'lazy': 0})
  call dein#load_toml(s:lazy_toml, {'lazy': 1})

  " 設定終了
  call dein#end()
  call dein#save_state()
endif

" もし、未インストールものものがあったらインストール
if dein#check_install()
  call dein#install()
endif

" ##############################################
" plugin settings 
" ##############################################

""" markdown {{{
  autocmd BufRead,BufNewFile *.mkd  set filetype=markdown
  autocmd BufRead,BufNewFile *.md  set filetype=markdown
  " Need: kannokanno/previm
  nnoremap <silent><leader>p :PrevimOpen<CR> " Ctrl-pでプレビュー
  " 自動で折りたたまないようにする
  let g:vim_markdown_folding_disabled=1
" }}}

" "vim_indent
" let g:ubdebt_guides_start_level = 2
" let g:ubdebt_guides_guide_level = 2
" let g:indent_guides_auto_colors = 0
" autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red   ctermbg=3
" autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=green ctermbg=4
" " auto_save
" let g:auto_save = 1
" let g:auto_save_silent = 1  " do not display the auto-save notification
" 
" " lexima
" call lexima#add_rule({'at': '\%#.*[-0-9a-zA-Z_,:]', 'char': '{', 'input': '{'})
" call lexima#add_rule({'at': '\%#\n\s*}', 'char': '}', 'input': '}', 'delete': '}'})
" 
" 
" ##############################################
" 表示系
" ##############################################

set fileencoding=utf-8 " 保存時の文字コード
set fileencodings=ucs-boms,utf-8,euc-jp,cp932 " 読み込み時の文字コードの自動判別. 左側が優先される
set fileformats=unix,dos,mac " 改行コードの自動判別. 左側が優先される
set ambiwidth=double " □や○文字が崩れる問題を解決

set t_Co=256

" tmux でvimで開いているファイル名を表示させる
if $TMUX != ""
  augroup titlesettings
    autocmd!
    autocmd BufEnter * call system("tmux rename-window " . "'[vim] " . expand("%:t") . "'")
    autocmd VimLeave * call system("tmux rename-window zsh")
    autocmd BufEnter * let &titlestring = ' ' . expand("%:t")
  augroup END
endif

set nowrap
set number

" invisibul character
set list
set listchars=tab:>-,trail:-,extends:»,precedes:«,nbsp:%

" cursor
set mouse=a
set guicursor=a:blinkon1
" set guicursor+=n-v-c:blinkon0
highlight Cursor guifg=whith guibg=red
set cursorline
autocmd InsertEnter,InsertLeave * set nocursorline!

" Change Color when entering Insert Mode
autocmd InsertEnter * highlight  Cursor ctermbg=Red ctermfg=Red
" Revert Color to default when leaving Insert Mode
autocmd InsertLeave * highlight  Cursor ctermbg=Yellow ctermfg=None
" cursor shape
if exists('$TMUX')
  let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
  let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
  let &t_SI = "\<Esc>]50;CursorShape=1\x7"
  let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif
" tab indent setting
set expandtab " タブ入力を複数の空白入力に置き換える
set tabstop=4 " 画面上でタブ文字が占める幅
set softtabstop=4 " 連続した空白に対してタブキーやバックスペースキーでカーソルが動く幅

set autoindent " 改行時に前の行のインデントを継続する
set smarttab
set smartindent " 改行時に前の行の構文をチェックし次の行のインデントを増減する
set shiftwidth=4 " smartindentで増減する幅

" use clipboard
set clipboard&
set clipboard+=unnamedplus
set clipboard=unnamedplus

" backup settings
set nobackup
set noswapfile

" コマンドの補完
set wildmenu " コマンドモードの補完
set history=5000 " 保存するコマンド履歴の数

" 検索系
set ignorecase " 検索文字列が小文字の場合は大文字小文字を区別なく検索する
set smartcase " 検索文字列に大文字が含まれている場合は区別して検索する
set incsearch " 検索文字列入力時に順次対象文字列にヒットさせる
set wrapscan " 検索時に最後まで行ったら最初に戻る
set hlsearch " 検索語をハイライト表示
" ESC連打でハイライト解除
nmap <Esc><Esc> :nohlsearch<CR><Esc>

set wildmenu " コマンドモードの補完
set history=5000 " 保存するコマンド履歴の数

set autoread
set hidden  "バッファを閉じる代わりに隠す(undo履歴を残すため)
set showcmd
set virtualedit=onemore  "行末の1文字先までカーソルを移動できるように

set visualbell  " ビープ音を可視化
set showmatch  "カッコ入力時の対応するカッコを表示
set matchtime=3  " 対応カッコのハイライト表示を3秒にする

" ##############################################
" key bindings
" ##############################################

" タブ関係
nnoremap s <Nop>

nnoremap se :<C-u>sp<CR>
nnoremap ss :<C-u>vs<CR>

nnoremap sj <C-w>j
nnoremap sk <C-w>k
nnoremap sl <C-w>l
nnoremap sh <C-w>h

nnoremap _ <C-w>-
nnoremap + <C-w>+
nnoremap ) <C-w>>
nnoremap ( <C-w><

nnoremap sn gt
nnoremap sp gT

nnoremap sr <C-w>r
nnoremap s= <C-w>=
nnoremap sw <C-w>w
nnoremap so <C-w>_<C-w>|
nnoremap sO <C-w>=
nnoremap sN :<C-u>bn<CR>
nnoremap sP :<C-u>bp<CR>
nnoremap st :<C-u>tabnew<CR>
nnoremap sT :<C-u>Unite tab<CR>

" " call submode#enter_with('bufmove', 'n', '', 's>', '<C-w>>')
" " call submode#enter_with('bufmove', 'n', '', 's<', '<C-w><')
" " call submode#enter_with('bufmove', 'n', '', 's+', '<C-w>+')
" " call submode#enter_with('bufmove', 'n', '', 's-', '<C-w>-')
" " call submode#map('bufmove', 'n', '', '>', '<C-w>>')
" " call submode#map('bufmove', 'n', '', '<', '<C-w><')
" " call submode#map('bufmove', 'n', '', '+', '<C-w>+')
" " call submode#map('bufmove', 'n', '', '-', '<C-w>-')


" 表示行単位で上下移動するように
nnoremap j gj
nnoremap k gk
nnoremap <Down> gj
nnoremap <Up>   gk


nnoremap <leader>/ :TComment<CR>

noremap <leader>d :VimFiler -split -simple -winwidth=20 -no-quit -auto-cd<ENTER><ENTER>
" nerdtreeのショートカット
" nnoremap <silent><leader>d :NERDTreeToggle<CR>
" let NERDTreeShowHidden = 1

nnoremap <leader>s :%s/
" nnoremap m/ 0i# <CR>
"実行のショートカット
autocmd BufNewFile,BufRead *.rb nnoremap <leader>r :w<enter>:!ruby %<enter>
autocmd BufNewFile,BufRead *.py nnoremap <leader>r :w<enter>:!python -B %<enter>
autocmd BufNewFile,BufRead *.pl nnoremap <leader>r :w<enter>:!perl %<enter>

nnoremap <leader>u :Unite file_mru<enter><CR>


" ##############################################
" 全角スペースをハイライト表示
" ##############################################
function! ZenkakuSpace()
    highlight ZenkakuSpace cterm=reverse ctermfg=DarkMagenta gui=reverse guifg=DarkMagenta
endfunction
       
if has('syntax')
    augroup ZenkakuSpace
        autocmd!
        autocmd ColorScheme       * call ZenkakuSpace()
        autocmd VimEnter,WinEnter * match ZenkakuSpace /　/
    augroup END
    call ZenkakuSpace()
endif


"********************************************************************
"  



" **************************************************
" lightline
set laststatus=2

let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'mode_map': {'c': 'NORMAL'},
      \ 'active': {
      \   'left': [ ['mode', 'paste', 'readonly'], ['fugitive'], ['anzu'] ]
      \ },
      \ 'component': {
      \   'lineinfo': '%3l:%-2v',
      \ },
      \ 'component_function': {
      \   'modified': 'MyModified',
      \   'readonly': 'MyReadonly',
      \   'fugitive': 'MyFugitive',
      \   'mode': 'MyMode',
      \   'anzu': 'anzu#search_status',
      \ }
      \ }

function! MyModified()
  return &ft =~ 'help\|vimfiler\|gundo' ? '' : &modified ? '+' : &modifiable ? '' : '-'
endfunction

function! MyReadonly()
  return &ft !~? 'help\|vimfiler\|gundo' && &readonly ? ' ' : ''
endfunction

function! MyFugitive()
  try
    if &ft !~? 'vimfiler\|gundo' && exists('*fugitive#head') && strlen(fugitive#head())
      return '[' . fugitive#head() . ']'
    endif
  catch
  endtry
  return ''
endfunction

function! MyMode()
  return winwidth(0) > 60 ? lightline#mode() : ''
endfunction
""""""""""""""""""""""""""""""
" function! s:open_memo_file(category)"
"     " let l:category = input('Category: ')
"     " let l:memo_dir = $HOME . '/memo/vim/' . l:category
" 
"     " if l:category == ""
"         " let l:category = "other"
"     " endif
"     if a:category == 'todo'
"         let l:memo_dir = s:todo_dir
"     elseif a:category == 'done'
"         let l:memo_dir = s:done_dir
"     elseif a:category == 'memo'
"         let l:memo_dir = s:memo_dir
"     else
"         echo "category is not exists!!!"
"         break
"     endif
" 
"     " let l:memo_dir = $HOME . '/vim_memo/vim/' . l:category
"     if !isdirectory(l:memo_dir)
"         call mkdir(l:memo_dir, 'p')
"     endif
" 
"     let l:filename = l:memo_dir . '/' . l:title . strftime('_%Y-%m-%d')
" 
"     let l:template = [
"                 \'Category: ' . a:category,
"                 \'========================================',
"                 \'Title: ' . l:title,
"                 \'----------------------------------------',
"                 \'date: ' . strftime('%Y/%m/%d %T'),
"                 \'- - - - - - - - - - - - - - - - - - - - ',
"                 \'',
"                 \]
" 
"     " ファイル生成
"     execute 'tabnew ' . l:filename
"     call setline(1, l:template)
"     execute '999'
"     execute 'write'
" endfunction augroup END"
