#!/bin/sh
echo " *********** start setup dotfiles ********** "

function MKDIR (){
  DIR=$1
  if [[ ! -d $DIR ]]; then
      mkdir $DIR
  else
      echo -e "$DIR\n already exists but is not a directory" 1>&2
  fi
}


function MkSymLink (){
  input_FILE_PATH=$HOME/dotfiles/$1
  output_FILE_DIR=$2
  FILE_NAME=`basename ${input_FILE_PATH}`

  ln -sf ${input_FILE_PATH} ${output_FILE_DIR}${FILE_NAME}
  echo " ---- made symLink ${output_FILE_DIR}${FILE_NAME}"
}
# # samples
# MKDIR $HOME/test_directory
# MkSymLink vim/dein.toml $HOME/test_directory/


# for vim settings
MKDIR $HOME/.vim/
MKDIR $HOME/.vim/rc/
MKDIR $HOME/.cache/dein/
MkSymLink vim/.vimrc $HOME/
MkSymLink vim/dein.toml $HOME/.vim/rc/
MkSymLink vim/dein_lazy.toml $HOME/.vim/rc/

# # for bash settings
# MkSymLink bash/.bashrc $HOME/
# MkSymLink bash/.profile $HOME/

# for zsh settings
MkSymLink zsh/.zshrc $HOME/
MkSymLink zsh/.zshenv $HOME/

# for git settings
MkSymLink git/.gitignore_global $HOME/

# for tmux settings
MkSymLink tmux/.tmux.conf $HOME/

echo " *********** end of setup dotfiles ********** "
