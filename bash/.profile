# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin directories
PATH="$HOME/bin:$HOME/.local/bin:$PATH"

#**********************************************************
# setting path for myself

# added by Anaconda3 4.4.0 installer
PATH="/home/hawk/anaconda3/bin:$PATH"
# CUDA_HOME="/usr/local/cuda-8.0"
# LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${CUDA_HOME}/lib64
# PATH=$PATH:${CUDA_HOME}/bin
PYTHONPATH="~/anaconda3/lib/python3.6/site-packages:$PYTHONPATH"

# added by Anaconda3 4.4.0 installer
PATH="/home/hawk/anaconda3/bin:$PATH"
XDG_CONFIG_HOME="$XDG_CONFIG_HOMEH:~/.config"

# myformatter for python
PATH="/home/hawk/myformatter/:$PATH"

#**********************************************************

